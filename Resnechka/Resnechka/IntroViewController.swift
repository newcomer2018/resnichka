//
//  IntroViewController.swift
//  Resnechka
//
//  Created by Umid on 3/22/19.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nameField.layer.cornerRadius = 15
        nameField.clipsToBounds = true
        nameField.layer.borderWidth = 1.5
        nameField.layer.borderColor = UIColor.init(red: 255/255, green: 84/255, blue: 0/255, alpha: 1).cgColor

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
   
    @IBAction func nextAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as!   LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
