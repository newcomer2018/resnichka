//
//  LoginViewController.swift
//  Resnechka
//
//  Created by Umid on 3/28/19.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var confPassField: UITextField!
  
    @IBOutlet weak var resetPassButton: UIButton!
    
    @IBOutlet weak var stateButton: UIButton!
    
    @IBOutlet weak var authButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        emailField.layer.cornerRadius = 15
        emailField.clipsToBounds = true
        emailField.layer.borderWidth = 1.5
        emailField.layer.borderColor = UIColor.init(red: 255/255, green: 84/255, blue: 0/255, alpha: 1).cgColor
        
        passwordField.layer.cornerRadius = 15
        passwordField.clipsToBounds = true
        passwordField.layer.borderWidth = 1.5
        passwordField.layer.borderColor = UIColor.init(red: 255/255, green: 84/255, blue: 0/255, alpha: 1).cgColor
        
        confPassField.layer.cornerRadius = 15
        confPassField.clipsToBounds = true
        confPassField.layer.borderWidth = 1.5
        confPassField.layer.borderColor = UIColor.init(red: 255/255, green: 84/255, blue: 0/255, alpha: 1).cgColor
    }
    

    @IBAction func signInstagramAction(_ sender: UIButton) {
    }
    

    @IBAction func signVkAction(_ sender: UIButton) {
    }
    
    @IBAction func signFacebookAction(_ sender: UIButton) {
    }
    
    
    @IBAction func signGmailAction(_ sender: Any) {
    }
    
    
    @IBAction func authAction(_ sender: UIButton) {
        
        let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
        UIView.transition(with: (self.view.window)!, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.view.window?.rootViewController = tabBarVC
           
        })
    }
    
    
    @IBAction func changeStateAction(_ sender: UIButton) {
    }
    
}
